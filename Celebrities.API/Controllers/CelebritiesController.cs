﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Celebrities.API.Resources;
using Celebrities.Core.Models;
using Celebrities.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace Celebrities.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CelebritiesController : ControllerBase
    {
        private readonly ICelebrityService celebrityService;
        private readonly IMapper mapper;

        public CelebritiesController(ICelebrityService celebrityService, IMapper mapper)
        {
            this.celebrityService = celebrityService;
            this.mapper = mapper;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<CelebrityResource>>> GetAll()
        {
            var celebrities = await celebrityService.GetAllCelebrities();
            var musicResources = mapper.Map<IEnumerable<Celebrity>, IEnumerable<CelebrityResource>>(celebrities);
            return Ok(musicResources);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CelebrityResource>>> GetCelebrityById(int id)
        {
            var celebrity = await celebrityService.GetCelebrityById(id);
            var celebrityResources = mapper.Map<Celebrity, CelebrityResource>(celebrity);
            return Ok(celebrityResources);
        }

        [HttpPost("")]
        public async Task<ActionResult<CelebrityResource>> CreateMusic([FromBody] SaveCelebrityResource saveCelebrityResource)
        {
            var celebrityToCreate = mapper.Map<SaveCelebrityResource, Celebrity>(saveCelebrityResource);
            var newCelebrity = await celebrityService.CreateCelebrity(celebrityToCreate);

            var celebrity = await celebrityService.GetCelebrityById(newCelebrity.ID);

            var celebrityResource = mapper.Map<Celebrity, CelebrityResource>(celebrity);

            return Ok(celebrityResource);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<CelebrityResource>> UpdateMusic(int id, [FromBody] SaveCelebrityResource saveCelebrityResource)
        {
            var celebrityToBeUpdated = await celebrityService.GetCelebrityById(id);

            if (celebrityToBeUpdated == null)
                return NotFound();

            var celebrity = mapper.Map<SaveCelebrityResource, Celebrity>(saveCelebrityResource);

            await celebrityService.UpdateCelebrity(celebrityToBeUpdated, celebrity);

            var updatedCelebrity = await celebrityService.GetCelebrityById(id);
            var updatedCelebrityResource = mapper.Map<Celebrity, CelebrityResource>(updatedCelebrity);

            return Ok(updatedCelebrityResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCelebrity(int id)
        {
            if (id == 0)
                return BadRequest();

            var celebrity = await celebrityService.GetCelebrityById(id);

            if (celebrity == null)
                return NotFound();

            await celebrityService.DeleteCelebrity(celebrity);

            return NoContent();
        }
    }
}
