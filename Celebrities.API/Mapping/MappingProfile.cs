﻿using AutoMapper;
using Celebrities.API.Resources;
using Celebrities.Core.Models;

namespace Celebrities.API.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to Resource
            CreateMap<Celebrity, CelebrityResource>();
            CreateMap<Celebrity, SaveCelebrityResource>();

            // Resource to Domain
            CreateMap<CelebrityResource, Celebrity>();
            CreateMap<SaveCelebrityResource, Celebrity>();
        }
    }
}
