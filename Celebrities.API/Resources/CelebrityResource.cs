﻿using Celebrities.Core.Models;
using System;

namespace Celebrities.API.Resources
{
    public class CelebrityResource
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public Roll Roll { get; set; }
        public Uri ImageUri { get; set; }
    }
}
