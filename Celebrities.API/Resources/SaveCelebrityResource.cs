﻿using Celebrities.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Celebrities.API.Resources
{
    public class SaveCelebrityResource
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public Roll Roll { get; set; }
        public Uri ImageUri { get; set; }
    }
}
