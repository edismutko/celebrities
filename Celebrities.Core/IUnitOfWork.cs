﻿using Celebrities.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Celebrities.Core
{
    public interface IUnitOfWork: IDisposable
    {
        ICelebrityRepository Celebrities { get; }
        Task<int> CommitAsync();
    }
}
