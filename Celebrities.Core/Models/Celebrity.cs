﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Celebrities.Core.Models
{
    public enum Gender { Unspecified = 0, Male = 1, Female = 2 }
    public enum Roll { Actor = 0, Director = 1, Producer = 2 }
    public class Celebrity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public Roll Roll { get; set; }
        public Uri ImageUri { get; set; }
    }
}
