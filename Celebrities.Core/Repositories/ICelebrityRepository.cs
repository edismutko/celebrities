﻿using Celebrities.Core.Models;

namespace Celebrities.Core.Repositories
{
    public interface ICelebrityRepository: IRepository<Celebrity>
    {
    }
}
