﻿using Celebrities.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Celebrities.Core.Services
{
    public interface ICelebrityService
    {
        Task<IEnumerable<Celebrity>> GetAllCelebrities();
        Task<Celebrity> GetCelebrityById(int id);
        Task<Celebrity> CreateCelebrity(Celebrity newCelebrity);
        Task UpdateCelebrity(Celebrity celebrityToBeUpdated, Celebrity celebrity);
        Task DeleteCelebrity(Celebrity celebrity);
    }
}
