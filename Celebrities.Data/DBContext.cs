﻿using Celebrities.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Celebrities.Data
{
    public class DBContext: IDisposable
    {
        public List<Celebrity> Celebrities { get; set; }

        public DBContext()
        { }

        public Task<int> SaveChangesAsync()
        {
            return Task.Run<int>(() => { return 1; /*Save to DB => return the number of records that were saved*/});
        }

        public void Dispose()
        {
            // Free db resources
        }
    }
}
