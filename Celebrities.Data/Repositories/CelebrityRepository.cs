﻿using Celebrities.Core.Models;
using Celebrities.Core.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Celebrities.Data.Repositories
{
    public class CelebrityRepository :  ICelebrityRepository
    {
        private DBContext context;

        public CelebrityRepository(DBContext context)
        {
            this.context = context;
        }

        public Task AddAsync(Celebrity entity)
        {
            return Task.Run(() => { context.Celebrities.Add(entity); });
        }

        public void Remove(Celebrity entity)
        {
            context.Celebrities.Remove(entity);
        }

        Task<IEnumerable<Celebrity>> IRepository<Celebrity>.GetAllAsync()
        {
            return Task.Run<IEnumerable<Celebrity>>(() => context.Celebrities);
        }

        ValueTask<Celebrity> IRepository<Celebrity>.GetByIdAsync(int id)
        {
            return new ValueTask<Celebrity>(Task.Run<Celebrity>(() => context.Celebrities.Find((celeb) => celeb.ID == id)));
        }
    }
}
