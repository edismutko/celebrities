﻿using Celebrities.Core;
using Celebrities.Core.Repositories;
using Celebrities.Data.Repositories;
using System.Threading.Tasks;

namespace Celebrities.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DBContext context;
        private CelebrityRepository celebrityRepository;

        public UnitOfWork()
        {
            this.context = new DBContext();
        }

        public ICelebrityRepository Celebrities => celebrityRepository = celebrityRepository ?? new CelebrityRepository(context);

        public async Task<int> CommitAsync()
        {
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
