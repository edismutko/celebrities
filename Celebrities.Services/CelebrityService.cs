﻿using Celebrities.Core;
using Celebrities.Core.Models;
using Celebrities.Core.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Celebrities.Services
{
    public class CelebrityService : ICelebrityService
    {
        private readonly IUnitOfWork unitOfWork;
        public CelebrityService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<Celebrity> CreateCelebrity(Celebrity newCelebrity)
        {
            await unitOfWork.Celebrities.AddAsync(newCelebrity);
            return newCelebrity;
        }

        public async Task DeleteCelebrity(Celebrity celebrity)
        {
            unitOfWork.Celebrities.Remove(celebrity);
            await unitOfWork.CommitAsync();
        }

        public async Task<IEnumerable<Celebrity>> GetAllCelebrities()
        {
            return await unitOfWork.Celebrities.GetAllAsync();
        }

        public async Task<Celebrity> GetCelebrityById(int id)
        {
            return await unitOfWork.Celebrities.GetByIdAsync(id);
        }

        public async Task UpdateCelebrity(Celebrity celebrityToBeUpdated, Celebrity celebrity)
        {
            celebrityToBeUpdated.Name = celebrity.Name;
            celebrityToBeUpdated.BirthDate = celebrity.BirthDate;
            celebrityToBeUpdated.Gender = celebrity.Gender;
            celebrityToBeUpdated.Roll = celebrity.Roll;
            celebrityToBeUpdated.ImageUri = celebrity.ImageUri;

            await unitOfWork.CommitAsync();
        }
    }
}
